## Upgrade

## If the specific versions are not further noted in this document, proceed as follows

- Shutdown the servlet container
- Replace both WAR files
- Start the servlet container
- Visit the MDR-GUI to check if a database upgrade is necessary, if so:

    - make a backup of your database
    - open the admin interface of the MDR-GUI
    - execute the database upgrade

After those steps the MDR should work.


## Upgrade from 1.7.x to 1.8.x

- The namespace of the configuration file changed:
    - open the `mdr.postgres.xml` file and replace every occurence of
      `config/PostgreSQL` with `common` (only the namespaces)

- proceed with the upgrade
